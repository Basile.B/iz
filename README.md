## iz

_iz_ was a general purpose library for the [D programming language](https://dlang.org/).
It was mostly an aggregate of unrelated utilities programmed while learning D.

It was only actively developped between 2014 and 2018 and later only  maintained because still used as dependency here and there,
notably by [Kheops](https://gitlab.com/basile.b/iz), a GUI library, and by dastworx, 
a program that was used by [dexed-ide](https://gitlab.com/basile.b/dexed).

Nowadays Kheops is itself abandoned and dexed still uses iz but to a very limited extent.
